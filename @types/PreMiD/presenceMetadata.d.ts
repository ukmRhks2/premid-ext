declare type presenceMetadata = {
	author: {
		id: string;
		name: string;
	};
	contributors?: {
		id: string;
		name: string;
	}[];
	service: string;
	altnames?: string[];
	description: { [langCode: string]: string };
	url: string | Array<string>;
	version: string;
	logo: string;
	thumbnail: string;
	color: string;
	tags: Array<string>;
	category: string;
	iframe?: boolean;
	readLogs?: boolean;
	regExp?: RegExp | string;
	iFrameRegExp?: RegExp | string;
	button?: boolean;
	warning?: boolean;
	settings: Array<{
		id: string;
		multiLanguage?: string | number | boolean;
		title?: string;
		icon?: string;
		if?: {
			[name: string]: string | number | boolean;
		};
		placeholder?: string;
		value?: string | number | boolean;
		values?: string[] | number[] | boolean[];
	}>;
};
