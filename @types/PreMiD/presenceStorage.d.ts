declare type presenceStorage = Array<{
	enabled: boolean;
	metadata: presenceMetadata;
	presence: string;
	iframe?: string;
	tmp?: boolean;
	metaTag?: boolean;
}>;
