import "./util/background/generic";
import "./util/background/onInstalled";
import "./util/background/onMessage";
import "./util/background/onStorageChange";
import "./util/background/onConnect";
import "./util/background/onUninstalled";

import { releaseType } from "./config";

if (["DEV", "ALPHA"].includes(releaseType)) {
	localStorage.debug = "PreMiD*";
}
