import { compare } from "compare-versions";
import clone from "lodash/cloneDeep";
import isEqual from "lodash/isEqual";

import { logger } from "../config";
import { getStorage, setStorage } from "./functions/util/asyncStorage";
import graphqlRequest, { getPresenceMetadata } from "./functions/util/graphql";
import {
	DEFAULT_LOCALE, getPresenceLanguages as presenceLanguages, getString, loadStrings, updateStrings
} from "./langManager";

const log = logger.extend("background").extend("presenceManager");
export interface platformType {
	os: string;
	arch: string;
}

export async function updatePresences() {
	log("Updating Presences...");

	let presenceVersions: Array<{
			name: string;
			version: string;
			url: string;
		}>,
		presences: presenceStorage = (await getStorage("local", "presences"))
			.presences;

	if (!presences || presences.length === 0)
		return log("No Presences, skipping...");

	//* Catch fetch error
	try {
		const graphqlResult = (
			await graphqlRequest(`
			query {
					presences(service: ["${presences
						.filter(p => !p.tmp)
						.map(p => p.metadata.service)
						.join('", "')}"]) {
					url
					metadata {
						service
						version
					}
				}
			}
		`)
		).data;

		presenceVersions = graphqlResult.presences.map(p => {
			return {
				name: p.metadata.service,
				url: p.url,
				version: p.metadata.version
			};
		});
	} catch (e) {
		return log(`Failed to update Presences: %o`, e);
	}

	let currPresenceVersions = presences
			.filter(p => !p.tmp)
			.map(p => {
				return {
					name: p.metadata.service,
					version: p.metadata.version
				};
			}),
		presencesToUpdate = currPresenceVersions.filter(p =>
			presenceVersions.find(
				p1 => p1.name == p.name && compare(p.version, p1.version, "<")
			)
		);

	if (presencesToUpdate.length === 0)
		return log("All Presences are up to date.");

	const graphqlResult = (
		await graphqlRequest(`
			query {
				presences(service: ["${presencesToUpdate.map(p => p.name).join('", "')}"]) {
					presenceJs
					iframeJs
					metadata {
						author {
							name
							id
						}
						contributors {
							name
							id
						}
						altnames
						warning
						readLogs
						service
						description
						url
						version
						logo
						thumbnail
						color
						tags
						category
						iframe
						regExp
						iframeRegExp
						button
						warning
						settings {
							id
							title
							icon
							if {
								propretyNames
								patternProprties
							}
							placeholder
							value
							values
							multiLanguage
						}
					}
				}
			}
			`)
	).data;

	for (const presence of graphqlResult.presences) {
		const installedPresence =
			presences[
				presences.findIndex(
					p => p.metadata.service === presence.metadata.service
				)
			];

		installedPresence.metadata = presence.metadata;
		installedPresence.presence = presence.presenceJs;
		installedPresence.iframe = presence.iframeJs;

		await initPresenceLanguages(presence);
		await checkPresenceSettingsIntegrity(presence);

		log(
			"Updated %s to %s",
			presence.metadata.service,
			presence.metadata.version
		);
	}

	await setStorage("local", { presences });
}

async function checkPresenceSettingsIntegrity(presence: presenceStorage[0]) {
	const internLog = log.extend(presence.metadata.service);

	let presenceSettings =
			(await getStorage("local", `pSettings_${presence.metadata.service}`))[
				`pSettings_${presence.metadata.service}`
			] || [],
		origSettings = clone(presenceSettings);

	//TODO Change
	if (!presence.metadata.settings) return true;

	const filterFn = (v: {
		value?: string | number | boolean;
		values?: string[] | number[] | boolean[];
	}) => {
		delete v.value;
		delete v.values;
	};

	presenceSettings = presenceSettings.filter(s => !s.multiLanguage);
	presence.metadata.settings = presence.metadata.settings.filter(
		s => !s.multiLanguage
	);

	presenceSettings.forEach(filterFn);
	presence.metadata.settings.forEach(filterFn);

	const removedSettings = presenceSettings
			.filter(s => !presence.metadata.settings.find(s1 => s1.id === s.id))
			.map(s => s.id),
		addedSettings = presence.metadata.settings
			.filter(s => !presenceSettings.find(s1 => s1.id === s.id))
			.map(s => s.id),
		changedSettings = presenceSettings
			.filter(
				s =>
					!isEqual(
						presence.metadata.settings.find(s1 => s1.id === s.id),
						s
					)
			)
			.map(s => s.id);

	if (removedSettings.length > 0) {
		internLog(
			"removed setting %s",
			presence.metadata.service,
			removedSettings.join(", ")
		);
		origSettings = origSettings.filter(s => !removedSettings.includes(s.id));
	}

	if (addedSettings.length > 0) {
		internLog(
			"missing setting %s",
			presence.metadata.service,
			addedSettings.join(", ")
		);
		addedSettings.forEach(s =>
			origSettings.push(presence.metadata.settings.find(s1 => s1.id === s))
		);
	}

	if (changedSettings.length > 0) {
		internLog(
			"changed setting %s",
			presence.metadata.service,
			changedSettings.join(", ")
		);

		origSettings.forEach(
			s => (s = presence.metadata.settings.find(s1 => s1.id === s))
		);
	}

	//TODO Refactor
	const langSettings = origSettings.find(
		s => typeof s.multiLanguage !== "undefined"
	);

	if (langSettings && langSettings.values) {
		const langs = langSettings.values.map(v => v.value);

		await Promise.all(langs.map(lang => loadStrings(lang, true)));
	}

	await setStorage("local", {
		[`pSettings_${presence.metadata.service}`]: origSettings
	});
}

export async function addPresence(name: string | string[]) {
	log(`Adding Presence %o`, name);
	let presences: presenceStorage = (await getStorage("local", "presences"))
		.presences;

	if (!presences) presences = [];
	//* Filter out tmp presences

	if (typeof name === "string") {
		if (presences.filter(p => !p.tmp).find(p => p.metadata.service === name)) {
			log(`Presence ${name} already added.`);
			return;
		}
	} else {
		const res = name.filter(
			s => !presences.map(p => p.metadata.service).includes(s)
		);

		if (res.length === 0) {
			log("Presences already added.");
			return;
		} else name = res;
	}

	if (typeof name === "string") {
		getPresenceMetadata(name)
			.then(async ({ data }) => {
				if (
					typeof data.metadata.button !== "undefined" &&
					!data.metadata.button
				)
					return;
				const presenceAndIframe = (
					await graphqlRequest(`
						query {
							presences(service: "${data.metadata.service}") {
    					presenceJs
    					iframeJs
    					}
						}
						`)
				).data.presences[0];

				const res: any = {
					metadata: data.metadata,
					presence: presenceAndIframe.presenceJs,
					enabled: true
				};

				if (typeof data.metadata.iframe !== "undefined" && data.metadata.iframe)
					res.iframe = presenceAndIframe.iframeJs;

				presences.push(res);
				chrome.storage.local.set({ presences: presences });
				presences.map(p => {
					if (p.metadata.settings) {
						chrome.storage.local.set({
							[`pSettings_${p.metadata.service}`]: p.metadata.settings
						});
					}
				});
			})
			.catch(() => {});
	} else {
		const presencesToAdd: any = (
			await Promise.all(
				(
					await Promise.all(
						name.map(name => {
							return getPresenceMetadata(name);
						})
					)
				).map(async ({ data }) => {
					if (
						typeof data.metadata.button !== "undefined" &&
						!data.metadata.button
					)
						return;

					const presenceAndIframe = (
						await graphqlRequest(`
						query {
							presences(service: "${data.metadata.service}") {
    					presenceJs
    					iframeJs
    					}
						}
						`)
					).data.presences[0];

					const res: any = {
						metadata: data.metadata,
						presence: presenceAndIframe.presenceJs,
						enabled: true
					};
					if (
						typeof data.metadata.iframe !== "undefined" &&
						data.metadata.iframe
					)
						res.iframe = presenceAndIframe.iframeJs;

					return res;
				})
			)
		).filter(p => typeof p !== "undefined");

		chrome.storage.local.set({ presences: presences.concat(presencesToAdd) });
		presences.concat(presencesToAdd).map(p => {
			if (p.metadata.settings) {
				chrome.storage.local.set({
					[`pSettings_${p.metadata.service}`]: p.metadata.settings
				});
				//* Not awaiting it, it could take a lot of time
				initPresenceLanguages(p);
			}
		});
	}

	updateStrings(chrome.i18n.getUILanguage());
}

//* Only add these if is not background page
if (document.location.pathname !== "/_generated_background_page.html") {
	//* Add extension - buggy, would remove it.
	document.addEventListener("DOMContentLoaded", () => {
		if (document.querySelector("#app"))
			document.querySelector("#app").setAttribute("extension-ready", "true");
	});

	window.addEventListener("PreMiD_AddPresence", (data: CustomEvent) => {
		addPresence([data.detail]);
	});

	window.addEventListener(
		"PreMiD_RemovePresence",
		async (data: CustomEvent) => {
			const { presences } = await getStorage("local", "presences");

			chrome.storage.local.set({
				presences: (presences as presenceStorage).filter(
					p => p.metadata.service !== data.detail
				)
			});

			updatePresences();
		}
	);

	window.addEventListener("PreMiD_GetPresenceList", sendBackPresences);

	//* On presence change update
	chrome.storage.onChanged.addListener(key => {
		if (Object.keys(key)[0] === "presences") sendBackPresences();
	});
}

async function sendBackPresences() {
	const presences = (await getStorage("local", "presences"))
		.presences as presenceStorage;
	let data = {
		detail: presences.filter(p => !p.tmp).map(p => p.metadata.service)
	};

	// @ts-ignore
	if (typeof cloneInto === "function")
		// @ts-ignore
		data = cloneInto(data, document.defaultView);

	const event = new CustomEvent("PreMiD_GetWebisteFallback", data);
	setTimeout(() => window.dispatchEvent(event), 100);
}

export async function initPresenceLanguages(p) {
	if (p.metadata.settings) {
		const lngSettingIdx = p.metadata.settings.findIndex(
			s => typeof s.multiLanguage !== "undefined"
		);

		if (lngSettingIdx >= 0) {
			const lngSetting = p.metadata.settings[lngSettingIdx],
				languages = await presenceMultiLanguageLanguages(
					lngSetting.multiLanguage,
					p.metadata.service
				);

			if (Object.keys(languages).length > 1) {
				await storeDefaultLanguageOfPresence(p, languages);
			} else {
				p.metadata.settings.splice(lngSettingIdx, 1);
			}
		}
	}
}

async function getPresenceLanguages(serviceName) {
	const values = [],
		languages = await presenceLanguages(serviceName);

	for (const language of languages) {
		values.push({
			name: await getString("name", language),
			value: language
		});
	}

	return values;
}

async function presenceMultiLanguageLanguages(multiLanguage, service) {
	switch (typeof multiLanguage) {
		case "boolean":
			if (multiLanguage === true) return await getPresenceLanguages(service);
			break;
		case "string":
			return await getPresenceLanguages(multiLanguage);
			break;
		case "object":
			if (multiLanguage instanceof Array) {
				let commonLngs = [];

				for (const prefix of multiLanguage) {
					if (typeof prefix === "string" && prefix.trim().length > 0) {
						const lngs = await getPresenceLanguages(prefix);

						//* Only load common languages
						if (commonLngs.length === 0) {
							commonLngs = lngs;
						} else {
							commonLngs = commonLngs.filter(
								cl => lngs.findIndex(l => l === cl) >= 0
							);
						}
					}
				}

				return commonLngs;
			}
			break;
	}
}

async function storeDefaultLanguageOfPresence(p, languages) {
	const lngSetting = p.metadata.settings.find(
		s => typeof s.multiLanguage !== "undefined"
	);

	let presenceSettings = (
		await getStorage("local", `pSettings_${p.metadata.service}`)
	)[`pSettings_${p.metadata.service}`];

	if (!presenceSettings && p.metadata.settings) {
		presenceSettings = p.metadata.settings;
	}

	if (
		!presenceSettings.find(
			s => s.id === lngSetting.id && s.values && s.values.length > 0
		)
	) {
		const uiLang = chrome.i18n.getUILanguage(),
			preferredValue = languages.find(l => l.value === uiLang);

		lngSetting.title = await getString("general.language", uiLang);
		lngSetting.icon = "fas fa-language";
		lngSetting.value = preferredValue ? preferredValue.value : DEFAULT_LOCALE;
		lngSetting.values = languages;

		const lngSettingIdx = presenceSettings.findIndex(
			s => s.id === lngSetting.id
		);
		presenceSettings[lngSettingIdx] = lngSetting;

		//* You may be wondering, why the fuck do you stringify and parse this? Guess what because Firefox sucks and breaks its storage
		chrome.storage.local.set(
			JSON.parse(
				JSON.stringify({
					[`pSettings_${p.metadata.service}`]: presenceSettings
				})
			)
		);
	}
}
