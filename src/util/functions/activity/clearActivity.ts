import { logger } from "../../../config";
import { socket } from "../../socketManager";
import { priorityTab, setPriorityTab } from "../../tabPriority";

const log = logger.extend("activity").extend("clearActivity");

export default function clearActivity(resetTabPriority = false) {
	//* Send debug
	//* If resetTabPriority
	//* Emit clearActivity to app
	log(`Clear Activity | ${resetTabPriority}`);
	if (resetTabPriority) {
		//* Try to clearInterval
		//* Set priority tab to null
		chrome.tabs.sendMessage(priorityTab, { tabPriority: false });
		setPriorityTab(null);
	}
	if (socket.connected) socket.emit("clearActivity");
}
