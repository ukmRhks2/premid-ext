import cloneDeep from "lodash/cloneDeep";

import { logger, releaseType } from "../../../config";
import { socket } from "../../socketManager";
import { getStorage } from "../util/asyncStorage";
import detectSafari from "../util/detectSafari";
import checkUpdate from "../util/updateChecker";

const log = logger.extend("setActivity");

export default async function setActivity(
	presence: any,
	settings: any = undefined
) {
	if (["ALPHA", "BETA"].includes(releaseType)) {
		const { authorizedBetaAlpha } = await getStorage(
			"local",
			"authorizedBetaAlpha"
		);

		if (!authorizedBetaAlpha) return;
	}

	const safari = await checkUpdate(detectSafari());
	if (safari.urgentUpdate && !safari.latestVersion) return false;

	if (!settings) settings = (await getStorage("sync", "settings")).settings;
	if (presence == null || !settings.enabled.value) return;

	const pTS = cloneDeep(presence);

	if (pTS.trayTitle && settings.titleMenubar.value && pTS.trayTitle.trim())
		pTS.trayTitle = pTS.trayTitle.trim();
	else pTS.trayTitle = "";

	if (pTS.presenceData.details)
		pTS.presenceData.details.length < 3
			? (pTS.presenceData.details = "   " + pTS.presenceData.details)
			: (pTS.presenceData.details = pTS.presenceData.details
					.slice(0, 128)
					.trim());
	if (pTS.presenceData.state)
		pTS.presenceData.state.length < 3
			? (pTS.presenceData.state = "   " + pTS.presenceData.state)
			: (pTS.presenceData.state = pTS.presenceData.state.slice(0, 128).trim());

	if (socket.connected) socket.emit("setActivity", pTS);
	log("setActivity %o", pTS);
}
