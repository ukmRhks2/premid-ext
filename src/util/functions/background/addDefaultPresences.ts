import { defaultPresences, logger } from "../../../config";
import { addPresence } from "../../presenceManager";
import { getStorage, setStorage } from "../util/asyncStorage";

const log = logger.extend("background").extend("addDefaultPresences");
export default async function addDefaultPresences() {
	log("Initializing...");
	const defaultAdded = (await getStorage("local", "defaultAdded")).defaultAdded;

	if (defaultAdded) return log("Already executed, skipping.");

	log(`Adding default Presences (${defaultPresences.join(", ")})`);

	try {
		await addPresence(defaultPresences);

		await setStorage("local", {
			defaultAdded: true
		});
		log("Added default presences.");
	} catch (err) {
		log("Error while adding default presences... %o", err);
	}
}
