import { logger } from "../../../config";
import { oldActivity } from "../../background/onConnect";
import { socket } from "../../socketManager";
import { priorityTab } from "../../tabPriority";
import clearActivity from "../activity/clearActivity";
import setActivity from "../activity/setActivity";
import { getStorage } from "../util/asyncStorage";

let settings = null;

const log = logger.extend("background").extend("initSettings");
export default async function() {
	log("Initializing settings...");

	const presences = (await getStorage("local", "presences")).presences;
	if (!presences)
		await new Promise<void>(resolve =>
			chrome.storage.local.set({ presences: [] }, resolve)
		);

	settings = (await getStorage("sync", "settings")).settings;
	if (typeof settings === "undefined") settings = {};

	initSetting("enabled", "popup.setting.enabled", 0);
	initSetting("autoLaunch", "popup.setting.autoLaunch", 1);
	initSetting("titleMenubar", "popup.setting.titleMenubar", 2);

	await new Promise<void>(resolve =>
		chrome.storage.sync.set({ settings }, resolve)
	);

	log("Initializing done.");
}

chrome.storage.onChanged.addListener(async changes => {
	//* Main settings change
	if (changes.settings) {
		const nSettings = Object.assign(
			{},
			...Object.keys(changes.settings.newValue).map(k => {
				return { [k]: changes.settings.newValue[k].value };
			})
		);

		if (nSettings.enabled) setActivity(oldActivity, changes.settings.newValue);
		else if (priorityTab !== null) clearActivity(true);

		if (socket.connected) socket.emit("settingUpdate", nSettings);
		return log("Main Settings changed");
	}

	//* Presence Changed
	if (!changes.presences) return;

	const oldValue = changes.presences.oldValue,
		updatedPresences = changes.presences.newValue.filter(np =>
			oldValue.find(ov =>
				(ov.metadata.service === np.metadata.service &&
					ov.metadata.version !== np.metadata.version) ||
				typeof ov.tmp === "undefined"
					? false
					: np.tmp
			)
		);

	for (const updatedPresence of updatedPresences) {
		const settings = updatedPresence.metadata.settings,
			pSettings = (
				await getStorage(
					"local",
					`pSettings_${updatedPresence.metadata.service}`
				)
			)[`pSettings_${updatedPresence.metadata.service}`];

		console.log(pSettings);

		chrome.storage.local.get(
			`pSettings_${updatedPresence.metadata.service}`,
			storageSettings => {
				storageSettings =
					storageSettings[`pSettings_${updatedPresence.metadata.service}`] ||
					[];

				settings.forEach(setting => {
					const storageSetting = storageSettings.find(s => s.id === setting.id);

					if (storageSetting && storageSetting.value !== null) {
						if (typeof setting.value === typeof storageSetting.value) {
							setting.value = storageSetting.value;
						} else if (storageSetting.multiLanguage) {
							setting = storageSetting;
						}
					}
				});

				chrome.storage.local.set(
					JSON.parse(
						JSON.stringify({
							[`pSettings_${updatedPresence.metadata.service}`]: settings
						})
					),
					() => {
						log(
							`Updated setting storage of ${updatedPresence.metadata.service}`
						);
					}
				);
			}
		);
	}
});

function initSetting(
	setting: string,
	string: string,
	position: number,
	option: any = true,
	show = true
) {
	if (typeof settings[setting] === "undefined") {
		cOption(setting, string, position, option, show);
		log("Created %s:%j", setting, option);
	} else log("Skipping %s, exists", setting);
}

function cOption(
	setting: string,
	string: string,
	position: number,
	option: boolean,
	show: boolean
) {
	if (!settings[setting]) {
		settings[setting] = {
			string: string,
			value: option,
			position: position
		};

		if (show) settings[setting].show = show;
	}
}
