import { logger } from "../../../config";

const log = logger.extend("tabHasPresence");

export default async function tabHasPresence(tabId: number) {
	const res = await new Promise<boolean[]>(resolve => {
		chrome.tabs.executeScript(
			tabId,
			{
				code: "try{PreMiD_Presence}catch(_){false}",
				runAt: "document_start"
			},
			resolve
		);
	});

	log(!res ? false : true);
	if (!res) return false;
	else return res[0];
}
