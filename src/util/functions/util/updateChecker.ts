import { compare } from "compare-versions";

import cleanObject from "./cleanObject";
import graphqlRequest from "./graphql";

export default async function checkUpdate(isSafari: boolean) {
	if (!isSafari) return { latestVersion: true, urgentUpdate: false };
	const result = await graphqlRequest(`
		query {
			versions {
				safari {
					version
					urgent
				}
			}
		}`);
	cleanObject(result.data);
	if (
		compare(
			result.data.versions.safari.version,
			chrome.runtime.getManifest().version_name,
			">"
		)
	) {
		const urgentUpdate = result.data.versions.safari.urgent;
		chrome.browserAction.setBadgeText({ text: "!" });
		chrome.browserAction.setBadgeBackgroundColor({ color: "#ff5050" });
		return { latestVersion: false, urgentUpdate };
	}

	chrome.browserAction.setBadgeText({ text: "" });
	return { latestVersion: true, urgentUpdate: false };
}
