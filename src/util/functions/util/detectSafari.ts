export default function detectSafari() {
	//* Detect Safari Browser and return true/false.
	return (
		navigator.userAgent.indexOf("Safari") !== -1 &&
		navigator.userAgent.indexOf("Chrome") === -1
	);
}
