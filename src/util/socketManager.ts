import * as socketIo from "socket.io-client";

import { logger, requiredAppVersion } from "../config";
import { oldActivity } from "./background/onConnect";
import setActivity from "./functions/activity/setActivity";
import presenceDevManager from "./functions/background/presenceDevManager";
import { getStorage, setStorage } from "./functions/util/asyncStorage";
import { priorityTab } from "./tabPriority";

const log = logger.extend("socketManager");

//* Create socket
export const socket = socketIo.connect("http://localhost:3020", {
		autoConnect: false,
		timeout: 40000
	}),
	connect = () => {
		log("Connecting to app...");
		socket.open();
	};

export let appVersion = 0;

let appVersionTimeout: number = null;

socket.on("connect", async () => {
	log("Connected to app!");

	chrome.browserAction.setBadgeText({ text: "" });

	//* Tell app to give us its version
	//* Start timeout if we don't receive version response
	socket.emit("getVersion");
	//TODO Find better way to do this
	appVersionTimeout = window.setTimeout(() => {
		//* Didn't get response, probably old version.
		appVersion = -1;
		log("Unsupported App version");
	}, 10000);

	let settings = (await getStorage("sync", "settings")).settings;
	settings = Object.assign(
		{},
		...Object.keys(settings).map(k => {
			return { [k]: settings[k].value };
		})
	);

	socket.emit("settingUpdate", settings);
});

socket.on("receiveVersion", (version: number) => {
	log("Got App version: %s", version);

	clearTimeout(appVersionTimeout);
	appVersion = version;

	if (!supportedAppVersion()) return log("Unsupported App version %s", version);
	else log("Supported App version %s", version);

	if (oldActivity) setActivity(oldActivity);

	chrome.runtime.sendMessage({ socket: socket.connected });

	if (priorityTab !== null)
		chrome.tabs.sendMessage(priorityTab, { tabPriority: true });
});

socket.on("disconnect", () => {
	log("Disconnected from App");

	chrome.browserAction.setBadgeText({ text: "!" });
	chrome.browserAction.setBadgeBackgroundColor({ color: "#e1e100" });

	chrome.runtime.sendMessage({ socket: socket.connected });

	//TODO Refactor
	chrome.storage.local.get("presences", ({ presences }) => {
		presences = (presences as presenceStorage).filter(p => !p.tmp);
		chrome.storage.local.set({ presences: presences });
	});

	if (priorityTab !== null)
		chrome.tabs.sendMessage(priorityTab, { tabPriority: false });
});

socket.on("localPresence", presenceDevManager);

socket.on(
	"discordUser",
	async (
		discordUser: {
			avatar: string;
			bot: boolean;
			discriminator: string;
			flags: number;
			id: string;
			premium_type: string;
			username: string;
		} | null
	) => {
		if (!discordUser) return;
		setStorage("local", { discordUser });
	}
);

export function supportedAppVersion() {
	if (appVersion >= requiredAppVersion || appVersion === 0) {
		chrome.browserAction.setBadgeText({ text: "" });
		return true;
	} else {
		chrome.browserAction.setBadgeText({ text: "!" });
		chrome.browserAction.setBadgeBackgroundColor({ color: "#ff5050" });
		return false;
	}
}
