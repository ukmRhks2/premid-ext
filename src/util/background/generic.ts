import { logger, releaseType } from "../../config";
import { getStorage } from "../../helper";
import clearActivity from "../functions/activity/clearActivity";
import addDefaultPresences from "../functions/background/addDefaultPresences";
import initSettings from "../functions/background/initSettings";
import consoleHeader from "../functions/util/consoleHeader";
import detectSafari from "../functions/util/detectSafari";
import hasAlphaBetaAccess from "../functions/util/hasAlphaBetaAccess";
import { updateStrings } from "../langManager";
import { addPresence, updatePresences } from "../presenceManager";
import { connect } from "../socketManager";
import { hideMetaTagPresences, priorityTab, tabPriority } from "../tabPriority";

const log = logger.extend("background").extend("generic");

export async function start() {
	await consoleHeader();

	log("Initializing PreMiD...");

	log("Release Type: %s", releaseType);
	//* Check alpha/beta access if releasyType equals ALPHA/BETA
	if (["ALPHA", "BETA"].includes(releaseType)) await hasAlphaBetaAccess();

	log("Set Badge Text & color");
	//TODO Move this
	chrome.browserAction.setBadgeText({ text: "!" });
	chrome.browserAction.setBadgeBackgroundColor({ color: "#e1e100" });

	//* Initialize settings, Add default presences, Update strings, Update presences
	//* Start updater intervals
	//*
	//* Connect to app
	await Promise.all([initSettings(), addDefaultPresences(), updatePresences()]);
	setInterval(() => {
		updatePresences();
		updateStrings(chrome.i18n.getUILanguage());
	}, 60 * 30 * 1000);
	connect();
	log("Initializing done.");
}

if (!detectSafari())
	chrome.runtime.onUpdateAvailable.addListener(() => chrome.runtime.reload());

chrome.tabs.onActivated.addListener(info => {
	log("onActivated %o", info);
	tabPriority();
});
chrome.tabs.onReplaced.addListener((added, removed) => {
	log("onReplaced %s > %s", removed, added);
	//* Only clear if tab is priorityTab
	if (priorityTab !== removed) return;
	clearActivity(true);
	hideMetaTagPresences();
});
chrome.tabs.onRemoved.addListener((tabId, info) => {
	log("onRemoved %s: %o", tabId, info);
	//* Only clear if tab is priorityTab
	if (priorityTab !== tabId) return;
	clearActivity(true);
	hideMetaTagPresences();
});
chrome.tabs.onUpdated.addListener((tabId, info, tab) => {
	log("onUpdated %s:%o %o", tabId, info, tab);
	tabPriority(info);
});
chrome.windows.onFocusChanged.addListener((windowId, filters) => {
	log("onFocusChanged %s: %o", windowId, filters);
	//* Can't change window
	//* Run tabPriority
	if (windowId === -1) return;
	tabPriority();
});

window["addPresenceLegacy"] = async (presence: string | string[]) => {
	await addPresence(presence);

	if (typeof presence === "object") presence = presence.join(", ");

	return console.log(
		`%cAdding Presence: %c${presence}`,
		"font-size:1rem;color:#99AAB5;",
		"font-size:1rem;font-weight:bold;color:rgb(70,200,70)"
	);
};

window["removePresenceLegacy"] = async (presence: string) => {
	log(`Removing presence %o`, presence);
	const { presences } = await getStorage("local", "presences");

	chrome.storage.local.set({
		presences: (presences as presenceStorage).filter(
			p => p.metadata.service !== presence
		)
	});
	updatePresences();

	return console.log(
		`%cRemoving Presence: %c${presence}`,
		"font-size:1rem;color:#99AAB5;",
		"font-size:1rem;font-weight:bold;color:rgb(255, 0, 0)"
	);
};
