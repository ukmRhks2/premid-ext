import * as debug from "debug";

export const apiBase = "https://api.premid.app/v3/",
	releaseType: "BETA" | "ALPHA" | "RELEASE" | "DEV" = "RELEASE",
	requiredAppVersion = 214,
	logger = debug("PreMiD"),
	defaultPresences = [
		"YouTube",
		"YouTube Music",
		"Netflix",
		"Twitch",
		"SoundCloud"
	];
